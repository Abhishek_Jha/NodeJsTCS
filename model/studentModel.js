var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;


var studentSchema = new Schema({
  
  email:{type: String},
  name: {type: String},
  contactNo: {type: String},
  DOB: {type: String},
  address: {type: String},
  standard: {type: String},
  education: {type: String},
  time:{type: Date, default: Date.now}
});

studentSchema.method('toJSON', function () {
  return {
    id: this._id,
    email:this.email,
    name: this.name,
    contactNo: this.contactNo,
    DOB: this.DOB,
    address: this.address,
    standard: this.standard,
    education:this.education,
    time:this.time
  
    
  };
});

module.exports = mongoose.model('student', studentSchema);