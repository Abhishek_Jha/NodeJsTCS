var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;


var marksSchema = new Schema({
  math: {type: Number},
  science: {type: Number},
  english: {type: Number},
  hindi: {type: Number},
  biology: {type: Number},
  time:{type: Date, default: Date.now},
  studentId:{type: ObjectId, ref:'student'}
});


marksSchema.method('toJSON', function () {
  return {
    id: this._id,
    math: this.math,
    science: this.science,
    english: this.english,
    hindi: this.hindi,
    biology: this.biology,
    studentId: this.studentId,
    time:this.time
    
  };
});
module.exports = mongoose.model('marks', marksSchema);

