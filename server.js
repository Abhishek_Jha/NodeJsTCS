var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var passport = require('passport');

var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config/config'); // get our config file
var student = require('./model/studentModel'); // get our mongoose model
var marks = require('./model/marks')
//require('./route/api')(app);
var User = require('./model/user');
//var pass = require('./config/passport');


// =======================
// configuration =========
// =======================
var port = process.env.PORT || 8000; // used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database
app.set('abcdefghijklmnopqurstuvwxyz', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize())
app.use(passport.session())

// use morgan to log requests to the console
app.use(morgan('dev'));


// cross-origine resource.

//res.header("Access-Control-Allow-Origin", "*");


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// =======================
// routes ================
// =======================
// basic route
app.get('/', function (req, res) {
  res.send('Hello!  http://localhost:' + port + '');
});

//=============================================================================
app.post('/signup', function (req, res) {
  if (!req.body.username || !req.body.password) {
    res.json({ success: false, msg: 'Please pass username and password.' });
  } else {
    var newUser = new User({
      username: req.body.username,
      password: req.body.password
    });
    // save the user
    newUser.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({ success: false, msg: 'Username already exists.' });
      }
      res.json({ success: true, msg: 'Successful created new user.' });
    });
  }
});
//=============================================================================
//User Login
app.post('/login', function (req, res) {
  User.findOne({
    username: req.body.username
  }, function (err, user) {
    if (err) throw err;

    if (!user) {
      res.status(401).send({ success: false, msg: 'Authentication failed. User not found.' });
    } else {
      // check if password matches
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          // if user is found and password is right create a token
          //var token = jwt.sign(user, config.secret);
          // return the information including token as JSON
          res.json({ success: true, msg: 'User found LoggedIn success.' });
        } else {
          res.status(401).send({ success: false, msg: 'Authentication failed. Wrong password.' });
        }
      });
    }
  });
});

//===================================================================================

//
//================================================================================

// //GET  ALL STUDEN MARKS
app.post('/studentMarks', function (req, res, next) {
  const studentMarks = new marks({
    math: req.body.math,
    science: req.body.science,
    english: req.body.english,
    hindi: req.body.hindi,
    biology: req.body.biology,
    studentid: req.body.id

  });
  studentMarks.save()
    .then(result => {
      console.log(result);
      res.status(200).json({
        message: 'data saved'
      })
    })
    .catch(err => {
      console.log(err);
    })
});

//=====================================================================================

// //post stident details
app.post('/studentDetails', function (req, res, next) {
  const studentDetails = new student({
    email: req.body.email,
    name: req.body.name,
    contactNo: req.body.contactNo,
    DOB: req.body.DOB,
    address: req.body.address,
    standard: req.body.standard,
    education: req.body.education,

  });
  studentDetails.save()
    .then(result => {
      console.log(result);
      res.status(200).json({
        message: 'data saved'


      })

    })
    .catch(err => {

      console.log(err);
    })
});

//================================================================================
//GET ALL STUDENT BY ID

app.get('/getStudent/:id', function (req, res, next) {
  // var token = getToken(req.headers);
  const id = req.params.id;
  console.log('id', req.param.id);
  student.findById(id)
    .exec()
    .then(docs => {
      console.log(docs);
      res.status(200).json({
message:'Success',
data:docs

      });

    })
    .catch(err => {
      console.log(err);
      res.status(500).json('Failed');

    });

});

//==================================================================================

app.delete('/delStudent/:id', function (req, res, next) {
  // var token = getToken(req.headers);
  const id = req.params.id;
  console.log('id', req.param.id);
  student.remove({id})
    .exec()
    .then(docs => {
      console.log(docs);
      res.status(200).json({
message:'Success',
data:docs

      });

    })
    .catch(err => {
      console.log(err);
      res.status(500).json('Failed');

    });

});

//==================================================================================
//GET ALL STUDENT DETAILS

app.get('/getStudent', function (req, res, next) {
  // var token = getToken(req.headers);
  if (req.body) {
    student.find(function (err, student) {
      if (err) return next(err);
      res.json(student);
    });
  } else {
    return res.status(403).send({ success: false, msg: 'Unauthorized.' });
  }
});
//================================================================================
//GET ALL STUDENT MARKS

app.get('/getStudentMarks', function (req, res, next) {
  // var token = getToken(req.headers);
  if (req.body) {
    marks.find(function (err, student) {
      if (err) return next(err);
      res.json(student);
    });
  } else {
    return res.status(403).send({ success: false, msg: 'Unauthorized.' });
  }

});
//===============================================================================
//GET STUDENTMARKSBYID
app.get('/getStudentMarks/:id', function (req, res, next) {
  // var token = getToken(req.headers);
  const id = req.params.id;
  console.log('id', req.param.id);
  marks.findById(id)
    .exec()
    .then(docs => {
      console.log(docs);
      res.status(200).json({

        message:'Success',
        Data:docs
      });

    })
    .catch(err => {
      console.log(err);
      res.status(500).json('Failed');

    });

});
// API ROUTES -------------------
// we'll get to these in a second



// =======================
// start the server ======
// =======================
app.listen(port);
console.log('Server is running at http://localhost:' + port);

module.exports = app;